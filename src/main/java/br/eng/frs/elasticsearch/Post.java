package br.eng.frs.elasticsearch;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;


public class Post {
    
    private static final String SITE = "hostname.com";
    private static final int    PORT = 54743;
    private static final String PATH = "/elasticsearch/.kibana54743/visualization/";
    
    public static void main(String[] args) {
        JSONObject params = new JSONObject();
        
        params.put("title", "culrsalve03");
        params.put("id", "memory");
        params.put("type", "visualization");
        
        JSONObject visState = new JSONObject();
        visState.put("title", "New Visualization 05");
        visState.put("type", "pie");
        
        JSONObject visStateParams = new JSONObject();
        visStateParams.put("shareYAxis", true);
        visStateParams.put("addTooltip", true);
        visStateParams.put("addLegend", true);
        visStateParams.put("isDonut", false);

        params.put("visState", visState);
        
        JSONArray aggsArray = new JSONArray();
        
        JSONObject aggs1 = new JSONObject();
        aggs1.put("id", "1");
        aggs1.put("type", "count");
        aggs1.put("schema", "metric");
        aggs1.put("params", new JSONObject());
        
        JSONObject aggs2 = new JSONObject();
        aggs2.put("id", "2");
        aggs2.put("type", "date_histogram");
        aggs2.put("schema", "split");
        JSONObject aggs2Params = new JSONObject();
        aggs2Params.put("field", "@timestamp");
        aggs2Params.put("interval", "auto");
        aggs2Params.put("customInterval", "2h");
        aggs2Params.put("min_doc_count", 1);
        aggs2Params.put("extended_bounds", new JSONObject());
        aggs2Params.put("row", true);
        aggs2.put("params", aggs2Params);
               
        aggsArray.put(aggs1);
        aggsArray.put(aggs2);
        
        visState.put("aggs", aggsArray);
        
        visState.put("listeners", new JSONObject());
        visState.put("title", "culrsalve03");
        params.put("visState", visState.toString());
        params.put("uiStateJSON", new JSONObject());
        params.put("description", "");
        params.put("version", 1);
        
        JSONObject kibanaSavedObjectMeta = new JSONObject();
        
        JSONObject searchSourceJSON = new JSONObject();
        searchSourceJSON.put("index", "orion-mylast*");
        
        JSONObject query = new JSONObject();
        JSONObject queryString = new JSONObject();
        queryString.put("query", "*");
        queryString.put("analyze_wildcard", true);
        query.put("query_string", queryString);
        queryString.put("filter", new JSONArray());
        
        
        searchSourceJSON.put("query", query);
        kibanaSavedObjectMeta.put("searchSourceJSON", searchSourceJSON.toString());
        
        params.put("kibanaSavedObjectMeta", kibanaSavedObjectMeta);
        
        
        CredentialsProvider credsProvider = new BasicCredentialsProvider();
        credsProvider.setCredentials(
                new AuthScope(SITE, PORT),
                new UsernamePasswordCredentials("user", "passwd"));
        CloseableHttpClient client = HttpClients.custom()
                .setDefaultCredentialsProvider(credsProvider)
                .build();
        
        HttpPost httppost = new HttpPost(
                "http://" + SITE + ":" + PORT + PATH
        );
        
        httppost.addHeader("kbn-version", "4.4.2");
        httppost.addHeader("Content-Type", "application/json");
        
        try {
            StringEntity json = new StringEntity(params.toString());
            json.setContentType("application/json");
            httppost.setEntity(json);
            CloseableHttpResponse response = client.execute(httppost);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(Post.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(-1);
        } catch (IOException ex) {
            Logger.getLogger(Post.class.getName()).log(Level.SEVERE, null, ex);
            System.exit(-1);
        }
    }
}
